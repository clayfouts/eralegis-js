/*
  eralegis.js -- Javascript helper library for rendering Era Legis dates

  Read documentation at https://eralegis.info/

  VERSION 3.0

  Copyright (c) 2012-2019 Clay Fouts

*/

'use strict';

( function () {

    var updateDate = function(element, success) {
        var params = {format: 'text'};
        var content = element.innerHTML;
        if ( content.match('\{') ) {
            // Assume noise unless there's a template token included
            params['template'] = content;
        }

        [ 'lang', 'tz', 'roman-year', 'show-terse', 'show-deg', 'show-dow', 'show-year', 'location']
            .forEach(function (name) {
                var attr = element.getAttribute('data-'+name);
                if (attr != null) {
                    params[name.replace(/-/g, '_')] = attr;
                }
            });

        if ( params['tz'] === undefined ) {
            var tzOffset = new Date().getTimezoneOffset();
            var tzSign = (tzOffset <= 0) ? '' : '-';
            var isoOffset = (Math.abs(tzOffset) / 60) * 100;
            for (var i = 4 - isoOffset.toString().length; i > 0 ; i--) {
                isoOffset = '0' + isoOffset;
            }
            params['tz'] = tzSign + isoOffset;
        }

        var doFetch = function(params) {
            var base_url = (window.eralegisBaseUrl) ? window.eralegisBaseUrl
                : 'https://date.eralegis.info/';
            var url = new URL(base_url + (element.getAttribute('data-evdate') || ''));
            Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

            fetch(url)
                .then( function(response) {
                    return response.text();
                })
                .then( function(text) {
                    success(element, text);
                });
        };

        if ( params['location'] === 'auto' ) {
            delete params['location'];

            navigator.geolocation.getCurrentPosition(
                function(pos) {
                    params['location'] = pos.coords.latitude + ':' + pos.coords.longitude;
                    doFetch(params);
                },
                function(err) {
                    console.log('Unable to fetch position: ' + err.message);
                    doFetch(params);
                });
        } else {
            doFetch(params);
        }

    }

    var onReady = function() {
        var spans = document.getElementsByClassName('date93');
        for(var i = 0; spans[i]; i++) {
            updateDate( spans[i], function(el, date) {
                el.innerHTML = date;
            });
        }
    }

    (document.readyState !== 'loading') ? onReady()
        : document.addEventListener('DOMContentLoaded', onReady);

})();
